Welcome, BIWIs!

This is the repository to easily manage the demo section of the CVL homepage:
http://www.vision.ee.ethz.ch/en/research/demos3/

as well some hints for the formatting for the CVL youtube channel:
https://www.youtube.com/channel/UCCN-FDWx3I01RshBqwSD2og [Subscribe! :p)

*****************************************************************

DEMOS: HOW TO USE THE FOLDER STRUCTURE

layout website cvl demos
  
    FOLDER
        * year
        * folder_nickname
        * icon.jpg icon.png or or icon.gif as 600x400 resolution

    HTML
        * title nickname
        * 2 line description (max 37 chars per line)
        * link to project page, code, video, data, paper on biwi server etc

    EXAMPLE 1

        VARCITY
        semantic and dynamic city modelling 
        building a city from images (2017)


    EXAMPLE 2

        FULLY BODY POSE
        real-time body pose estimation using 
        3d haarlets (2009)




*****************************************************************

VIDEOS: HOW TO CREATE VIDEOS FOR THE YOUTUBE CHANNEL

layout youtube cvl videos

    VIDEO
        * ETH SEQUENCE
        * CVL SEQUENCE
        * OVERVIEW SEQUENCE: (skip, crop old intro slides)

        * TITLE SEQUENCE (Title Always In Wordwise Caps):
            Title
            Authors
            Conference Year YYYY

        * VIDEO (concat multiple videos)

        * CVL SEQUENCE
        * ETH SEQUENCE


    DESCRIPTION
        * Title
        * Authors
        * Conference Year YYYY

        * Acknowledgements

        * Computer Vision Lab, ETH Zurich
        * http://www.vision.ee.ethz.ch/



    EXAMPLE 1

    *    ETH SEQUENCE
    *    CVL SEQUENCE
    *    OVERVIEW SEQUENCE: (skip, crop old intro slides)

    *    TITLE SEQUENCE (Title Always In Wordwise Caps):
            Appearances Can Be Deceiving: Learning Visual Tracking From Few Trajectory Annotations
            Santiago Manen, Junseok Kwon, Matthieu Guillaumin, Luc Van Gool
            ECCV 2014

    *    CVL SEQUENCE
    *    ETH SEQUENCE


    DESCRIPTION:
        Appearances Can Be Deceiving: Learning Visual Tracking From Few Trajectory Annotations
        Santiago Manen, Junseok Kwon, Matthieu Guillaumin, Luc Van Gool
        ECCV 2014

        Visual tracking is the task of estimating the trajectory of an
        object in a video given its initial location. This is usually done by combining
        [...]
        location error and improves the success rate by about 10%.


        The authors gratefully acknowledge support by Toyota.
        This work was supported by the ERC Advanced Grant VarCity (#273940).
        https://varcity.ethz.ch/

        Computer Vision Lab, ETH Zurich
        http://www.vision.ee.ethz.ch/


README FOR BITBUCKET


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).